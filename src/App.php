<?php
namespace Eco;

require_once __DIR__ . '/bootstrap.php';

use DebugBar\StandardDebugBar;
use Eco\Middleware\DispatchMiddleware;
use Eco\Middleware\LayoutMiddleware;
use Eco\Middleware\RouterMiddleware;
use Eco\Middleware\ViewMiddleware;
use Eco\ModuleManager\ModuleManager;
use Eco\ViewEngine\PhpEngine;
use Middlewares\Debugbar;
use Middlewares\Whoops;
use Psr\Http\Message\ServerRequestInterface;

class App {

    protected $modules;

    protected $middlewares;

    protected $isDev = false;

    /**
     * @var array
     */
    protected $config;

    protected $debugBar;

    public function __construct(array $modules) {
        // Load modules
        $moduleManager = new ModuleManager($modules);

        // Get modules names and paths from MM
        foreach ($moduleManager->getModules() as $moduleName => $module) {
            $this->modules [$moduleName] = $module->getPath();
        }

        // Config from modules
        $this->config = $moduleManager->getConfig();

        //
        $this->debugBar = $this->getDebugBar();
    }

    public function getModules() {
        return $this->modules;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setIsDev($isDev = true) {
        if ($isDev === true) {
            $this->debugBar = new StandardDebugBar();
        }
        $this->isDev = $isDev;
    }

    public function getDefaultMiddlewares() {
        $result = [
            new RouterMiddleware($this->config['router']),
            new DispatchMiddleware(),
            new ViewMiddleware(
                new PhpEngine($this->modules)
            ),
            new LayoutMiddleware(
                new PhpEngine($this->modules),
                $this->config['layout']
            )
        ];

        if ($this->isDev) {
            array_unshift(
                $result,
                new Whoops(),
                new Debugbar($this->getDebugBar())
            );
        }

        return $result;
    }

    public function getDebugBar() {
        return $this->debugBar;
    }

    public function dispatch(ServerRequestInterface $request, array $middlewares = []) {
        if (count($middlewares) === 0) {
            $middlewares = $this->getDefaultMiddlewares();
        }

        $dispatcher = new Dispatcher(
            $middlewares
        );

        return $dispatcher->handle($request);
    }
}
