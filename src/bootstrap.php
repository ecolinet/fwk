<?php

use Psr\Http\Message\ResponseInterface;

// Error to exceptions
set_error_handler(function ($errno, $errstr, $errfile, $errline, array $errcontext) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

// Escape
function e($vars) {
    foreach (func_get_args() as $arg) {
        echo htmlentities($arg, ENT_NOQUOTES, "UTF-8");
    }
}

// web root
function web_root() {
    static $webRoot;
    if (!$webRoot) {
        $webRoot = dirname(str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']));
        if (strlen($webRoot) === 0) {
            $webRoot .= '/';
        }
    }
    return $webRoot;
}

// Render Response
function render(ResponseInterface $response) {
    header("HTTP/" . $response->getProtocolVersion() . ' ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase());

    foreach ($response->getHeaders() as $headerName => $headerValues) {
        foreach ($headerValues as $headerValue) {
            header(ucfirst($headerName) . ': ' . $headerValue);
        }
    }

    echo $response->getBody();
}
