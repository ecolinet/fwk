<?php

namespace Eco;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Dispatcher implements RequestHandlerInterface {
    public $queue;

    public function __construct(array $queue) {
        $this->queue = $queue;
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface {
        $middleware = array_shift($this->queue);

        if ($middleware instanceof MiddlewareInterface) {
            return $middleware->process($request, $this);
        } else if (is_callable($middleware)) {
            return call_user_func($middleware, $request, $this);
        } else {
            throw new \RuntimeException("Invalid middleware");
        }
    }
}