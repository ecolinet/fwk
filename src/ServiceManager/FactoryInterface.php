<?php

namespace Eco\ServiceManager;

use Psr\Container\ContainerInterface;

interface FactoryInterface {
    /**
     * @param \Psr\Container\ContainerInterface $container
     * @return mixed
     */
    public function build(ContainerInterface $container);
}
