<?php
$vendorPath = realpath(__DIR__ . '/..');
while (!is_dir($vendorPath . '/vendor')) {
    $vendorPath = realpath($vendorPath . '/..');
    if ($vendorPath == '/') {
        throw new RuntimeException("Can't find vendor path");
    }
}

require $vendorPath . '/vendor/autoload.php';
