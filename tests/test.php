<?php
require __DIR__ . 'bootstrap.php';

class PDOFactory implements \Eco\ServiceManager\FactoryInterface
{
    public function build(\Psr\Container\ContainerInterface $locator)
    {
        $dbConfig = $locator->get('config')['db'];

        $db = new PDO(
            $dbConfig['dsn'],
            $dbConfig['username'],
            $dbConfig['password'],
            isset($dbConfig['options']) ?: []
        );
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
    }
}


$definitions = [
    'config' => [
        'db' => [
            'dsn' => 'mysql://localhost',
            'username' => 'root',
            'password' => 'password'
        ]
    ],
    'db' => PDOFactory::class
];


$sm = new \Eco\ServiceManager\ServiceManager();
$sm->addServicesDefinitions($definitions);

/** @var PDO $db */
$db = $sm->get('db');

var_dump(
    $db
        ->query("SHOW SCHEMAS")
        ->fetchAll()
);
